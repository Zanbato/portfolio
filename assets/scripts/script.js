let pie = document.querySelectorAll('.pie');

pie.forEach(function(pie){
	let value = pie.getAttribute('value');
	let innerText = pie.firstElementChild;
	innerText.innerHTML = value + '%';
	value = 100 - value;
	pie.style.animationDelay = '-'+value+'s';
})

